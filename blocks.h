//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "dwmblocks_audio.sh",                                              0,             10},
	{"", "dwmblocks_battery.sh",                                           20,              0},
	{" ", "date '+%H:%M %d-%m-%Y'",                                       60,              0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
